import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PatientsService } from './patients.service';
import { CreatePatientDto } from './dto/create-patient.dto';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';
import { AuthGuard } from '@nestjs/passport';
import { SendPollsPatientDto } from './dto/send-polls.dto';
import { UpdatePatientDto } from './dto/update-patient.dto';

@ApiTags('Patients')
@Controller({
  path: 'patients',
  version: '1',
})
@UseInterceptors(TransformationInterceptor)
export class PatientsController {
  constructor(private service: PatientsService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async findAll(@Request() request) {
    return this.service.findAll(request.user.id);
  }

  @Get('client/one')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  findPatientByUser(@Request() request) {
    return this.service.findPatientByUser(request.user.id);
  }

  @Patch('client/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  updatePatient(
    @Param('id') id: number,
    @Body() updatePatientDto: UpdatePatientDto,
    @Request() request,
  ) {
    return this.service.updatePatientById(
      +id,
      updatePatientDto,
      request.user.id,
    );
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  doctorUpdatePatient(
    @Param('id') id: number,
    @Body() updatePatientDto: UpdatePatientDto,
    @Request() request,
  ) {
    return this.service.doctorUpdatePatientById(
      +id,
      updatePatientDto,
      request.user.id,
    );
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  findPatient(@Param('id') id: string, @Request() request) {
    return this.service.findPatient(request.user.id, +id);
  }

  @Post('/:id/add-trainer')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  bindTrainersByUser(
    @Body() { ids }: { ids: string[] },
    @Param('id') patientId: string,
  ) {
    return this.service.bindTrainersByPatient(+patientId, ids);
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  createPatient(
    @Body() createPatientDto: CreatePatientDto,
    @Request() request,
  ) {
    return this.service.create(request.user.id, createPatientDto);
  }

  @Post(':id/send')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  sendPolls(
    @Body() sendPollsPatientDto: SendPollsPatientDto,
    @Request() request,
  ) {
    return this.service.sendPollsPatient(request.user.id, sendPollsPatientDto);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  deletePatient(@Param('id') id: string) {
    return this.service.softDeleteByPatient({ id: +id });
  }
}
