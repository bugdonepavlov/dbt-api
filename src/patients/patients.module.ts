import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Patient } from './entities/patient.entity';
import { PatientsController } from './patients.controller';
import { PatientsService } from './patients.service';
import { UsersModule } from 'src/users/users.module';
import { DoctorsModule } from 'src/doctors/doctors.module';
import { DoctorsNotesModule } from 'src/doctor_notes/doctor_notes.module';
import { PollsModule } from 'src/polls/polls.module';
import { AuthModule } from 'src/auth/auth.module';
import { TrainersModule } from 'src/trainers/trainers.module';
import { ForgotModule } from 'src/forgot/forgot.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Patient]),
    UsersModule,
    DoctorsModule,
    DoctorsNotesModule,
    PollsModule,
    AuthModule,
    TrainersModule,
    ForgotModule,
  ],
  controllers: [PatientsController],
  providers: [PatientsService],
  exports: [PatientsService],
})
export class PatientsModule {}
