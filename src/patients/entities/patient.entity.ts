import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EntityHelper } from 'src/utils/entity-helper';
import { User } from 'src/users/entities/user.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Status } from 'src/statuses/entities/status.entity';

@Entity()
export class Patient extends EntityHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  middleName?: string;

  @Column({ nullable: true })
  phone?: string;

  @Column({ nullable: true })
  telegram?: string;

  @Column()
  isLocal: boolean;

  @Column({ nullable: true })
  isPause: boolean;

  @Column({ type: 'timestamp', nullable: true })
  pauseEndDate: string | null;

  @Column()
  isDelete: boolean;

  @ManyToOne(() => Status, {
    eager: true,
  })
  status: Status;

  @ManyToOne(() => User, {
    eager: true,
  })
  user: User;

  @ManyToMany(() => Doctor, (doctor) => doctor.patients)
  @JoinTable({ name: 'patients_to_doctors' })
  doctors: Doctor[];

  @Column({
    type: 'jsonb',
    array: false,
    default: () => "'[]'",
    nullable: false,
  })
  trainers: string[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
