import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SendPollsPatientDto {
  @ApiProperty({ example: ['1', '2'] })
  @IsNotEmpty()
  polls: string[];

  @ApiProperty({ example: '3' })
  @IsNotEmpty()
  patientId: number;
}
