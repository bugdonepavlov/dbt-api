import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional } from 'class-validator';

export class UpdatePatientDto {
  @ApiProperty({ example: 'John' })
  @IsOptional()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsOptional()
  lastName: string;

  @ApiProperty({ example: 'Doe' })
  @IsOptional()
  middleName: string;

  @ApiProperty({ example: '@telega' })
  @IsOptional()
  telegram: string;

  @ApiProperty({ example: '+7(999) 888 88 88' })
  @IsOptional()
  phone: string;

  @ApiProperty({ example: 'Note' })
  @IsOptional()
  note: string;

  @ApiProperty({ example: 'test1@example.com' })
  @IsOptional()
  @IsEmail()
  email: string;
}
