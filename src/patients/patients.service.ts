import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UsersService } from 'src/users/users.service';
import { RoleEnum } from 'src/roles/roles.enum';
import { StatusEnum } from 'src/statuses/statuses.enum';
import { Role } from 'src/roles/entities/role.entity';
import { Status } from 'src/statuses/entities/status.entity';
import { generateHash, generatePassword } from 'src/utils/helpers';
import { DoctorsService } from 'src/doctors/doctors.service';
import { DoctorsNotesService } from 'src/doctor_notes/doctor_notes.service';
import { PollsService } from 'src/polls/polls.service';

import { Patient } from './entities/patient.entity';
import { CreatePatientDto } from './dto/create-patient.dto';
import { SendPollsPatientDto } from './dto/send-polls.dto';
import { EntityCondition } from 'src/utils/types/entity-condition.type';
import { AuthService } from 'src/auth/auth.service';
import { TrainersService } from 'src/trainers/trainers.service';
import { UpdatePatientDto } from './dto/update-patient.dto';
import { ForgotService } from 'src/forgot/forgot.service';

@Injectable()
export class PatientsService {
  constructor(
    @InjectRepository(Patient)
    private patientsRepository: Repository<Patient>,
    private usersService: UsersService,
    private doctorsService: DoctorsService,
    private doctorNotesServce: DoctorsNotesService,
    private pollsService: PollsService,
    private authService: AuthService,
    private trainersService: TrainersService,
    private forgotService: ForgotService,
  ) {}

  async create(userId: number, createPatientDto: CreatePatientDto) {
    const { email, note, ...rest } = createPatientDto;
    const password = generatePassword();
    const hash = generateHash();

    const doctor = await this.doctorsService.findOne({
      user: {
        id: userId,
      },
    });
    const user = await this.usersService.create({
      password,
      email,
      role: {
        id: RoleEnum.user,
      } as Role,
      status: {
        id: StatusEnum.inactive,
      } as Status,
      hash,
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'doctorNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const createPatient = this.patientsRepository.create({
      ...rest,
      user,
      isPause: false,
      isDelete: false,
      isLocal: true,
      doctors: [doctor],
      status: {
        id: StatusEnum.draft,
        name: 'draft',
      },
    });

    const patient = await this.patientsRepository.save(createPatient);

    if (note) {
      await this.doctorNotesServce.save({ patient, doctor, note });
    }

    const { firstName, lastName, id, isLocal, createdAt, status } = patient;

    return {
      isLocal,
      createdAt,
      id,
      name: `${firstName} ${lastName}`,
      note: note || '',
      status,
      user: {
        email: user.email,
        id: user.id,
        status: user.status,
      },
    };
  }

  async bindTrainersByPatient(patientId: number, ids: string[]) {
    await this.patientsRepository.update(patientId, { trainers: ids });

    return HttpStatus.OK;
  }

  async findPatientByUser(userId: number) {
    const patient = await this.findOne({
      user: {
        id: userId,
      },
    });

    if (!patient) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'patientNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    if (patient.status?.id === StatusEnum.invited) {
      await this.patientsRepository.update(patient.id, {
        status: {
          id: StatusEnum.active,
        },
      });
    }

    const {
      trainers: trainersIds,
      doctors,
      firstName,
      lastName,
      middleName,
      phone,
      telegram,
    } = patient;

    const trainers = trainersIds.map((trainerId) =>
      this.trainersService.findOne({ id: +trainerId }),
    );
    const patientTrainers = await Promise.all(trainers);

    return {
      id: patient.id,
      status: patient.status,
      trainers: patientTrainers,
      doctors,
      firstName,
      lastName,
      middleName,
      phone,
      telegram,
    };
  }

  async findOne(fields: EntityCondition<Patient>) {
    return await this.patientsRepository.findOne({
      relations: {
        doctors: true,
        user: true,
      },
      where: fields,
    });
  }

  async findPatient(userId: number, patientId: number) {
    const doctor = await this.doctorsService.findOne({
      user: {
        id: userId,
      },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'doctorNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const patient = await this.findOne({
      id: patientId,
      doctors: { id: doctor.id },
    });

    if (!patient) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'patientNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const {
      user,
      firstName,
      lastName,
      middleName,
      telegram,
      phone,
      createdAt,
      isDelete,
      isLocal,
      isPause,
    } = patient;
    const doctorNote = await this.doctorNotesServce.findOne(
      doctor.id,
      patient.id,
    );
    const patientTrainers = patient.trainers?.map((el) => Number(el));
    const forgot = await this.forgotService.findOneByUser(Number(user.id));
    const hash = forgot?.hash;
    console.log('====user.id', user.id);

    return {
      id: patient.id,
      firstName,
      lastName,
      middleName,
      telegram,
      phone,
      status: patient.status,
      createdAt,
      isPause,
      isDelete,
      isLocal,
      trainers: patientTrainers,
      name: `${firstName} ${lastName}`,
      user: {
        id: user.id,
        email: user.email,
        status: user.status,
        hash,
      },
      note: doctorNote?.note || '',
    };
  }

  async findAll(doctorId: number) {
    const doctor = await this.doctorsService.findOne({
      user: { id: doctorId },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'doctorNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const data = await this.patientsRepository.find({
      relations: {
        doctors: true,
        user: true,
      },
      where: {
        doctors: {
          id: doctor.id,
        },
      },
      order: {
        isDelete: 'ASC',
        firstName: 'ASC',
      },
    });

    const callbackData = async ({
      id,
      user,
      firstName,
      lastName,
      ...rest
    }: Patient) => {
      const doctorNote = await this.doctorNotesServce.findOne(doctorId, id);

      return {
        ...rest,
        id,
        name: `${firstName} ${lastName}`,
        note: doctorNote?.note || '',
        user: {
          email: user.email,
          id: user.id,
          status: user.status,
        },
      };
    };

    return Promise.all(
      data.filter(({ isDelete, user }) => !isDelete || user).map(callbackData),
    );
  }

  async sendPollsPatient(
    userId: number,
    { patientId, polls }: SendPollsPatientDto,
  ) {
    const doctor = await this.doctorsService.findOne({
      user: {
        id: userId,
      },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'doctorNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const patient = await this.patientsRepository.findOne({
      where: {
        doctors: {
          id: doctor.id,
        },
        id: patientId,
      },
    });

    if (!patient) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'patientNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const {
      id,
      user: { email },
      status,
    } = patient;
    let hash: string | undefined;

    await this.pollsService.saveDisabledPolls(polls, patientId);

    if (status.id === StatusEnum.draft && email) {
      await this.patientsRepository.update(
        { id },
        {
          status: {
            id: StatusEnum.invited,
          },
        },
      );

      hash = await this.authService.forgotPassword(email);
    }

    return hash;
  }

  async doctorUpdatePatientById(
    id: number,
    payload: UpdatePatientDto,
    userId: number,
  ) {
    const { email, note, ...rest } = payload;

    const doctor = await this.doctorsService.findOne({
      user: {
        id: userId,
      },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'doctorNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.patientsRepository.update(id, rest);

    const patient = await this.patientsRepository.findOne({
      where: {
        id,
      },
      relations: {
        user: true,
      },
    });

    if (email && patient && patient?.user.email !== email) {
      await this.usersService.updateEmail(patient.user.id, email);
    }

    if (patient && note && note.length) {
      await this.doctorNotesServce.update({
        patientId: patient.id,
        doctorId: doctor.id,
        note,
      });
    }

    return patient;
  }

  async updatePatientById(
    id: number,
    payload: UpdatePatientDto,
    doctorId?: number,
  ) {
    const { email, ...rest } = payload;

    const patient = await this.patientsRepository.save(
      this.patientsRepository.create({
        id,
        ...rest,
      }),
    );

    if (email) {
      const doctor = await this.doctorsService.findOne({
        id: doctorId,
      });
      doctor && (await this.usersService.updateEmail(patient.user.id, email));
    }

    return patient;
  }

  async softDeleteByPatient(fields: EntityCondition<Patient>) {
    const patient = await this.findOne(fields);

    if (!patient) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.usersService.softDelete(patient.user.id);

    return this.patientsRepository.delete(patient.id);
  }
}
