import { faker } from '@faker-js/faker';
import { ETypeInput } from 'src/polls/polls.interfaces';
import { pollsItems } from 'src/polls/utils';
import { StatusEnum } from 'src/statuses/statuses.enum';

export const doctors = [
  {
    firstName: 'Виталя',
    lastName: 'Казаков',
    email: 'vitaliykazalov@gmail.com',
    password: 'secret',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
  {
    firstName: 'Leha',
    lastName: 'Admin',
    email: 'lehaadmin@example.com',
    password: 'secret1',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
];

export const patients = [
  {
    parentId: 1,
    isLocal: true,
    firstName: 'Stephenson',
    lastName: 'Dawson',
    email: 'stephensondawson@amril.com',
    phone: '+1 (999) 496-3797',
    password: 'patient',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
  {
    parentId: 1,
    isLocal: false,
    firstName: 'Suzette',
    lastName: 'Oconnor',
    email: 'suzetteoconnor@amril.com',
    phone: '+1 (948) 490-3925',
    password: 'patient',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
  {
    parentId: 1,
    isLocal: false,
    firstName: 'Jayne',
    lastName: 'Bernard',
    email: 'jaynebernard@amril.com',
    phone: '+1 (919) 569-3106',
    password: 'patient',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
  {
    parentId: 1,
    isLocal: false,
    firstName: 'Byrd',
    lastName: 'Pearson',
    email: 'byrdpearson@amril.com',
    phone: '+1 (823) 536-3623',
    password: 'patient',
  },
  {
    parentId: 1,
    isLocal: false,
    firstName: 'Henderson',
    lastName: 'Brown',
    email: 'hendersonbrown@amril.com',
    phone: '+1 (836) 401-2120',
    password: 'patient',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
  {
    parentId: 1,
    isLocal: true,
    firstName: 'Tania',
    lastName: 'Carr',
    email: 'taniacarr@amril.com',
    phone: '+1 (963) 413-3285',
    password: 'patient',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
  {
    parentId: 1,
    isLocal: true,
    firstName: 'Robinson',
    lastName: 'Malone',
    email: 'robinsonmalone@amril.com',
    phone: '+1 (899) 441-2322',
    password: 'patient',
    status: {
      id: StatusEnum.active,
      name: 'Active',
    },
  },
];

const getFields = () =>
  pollsItems
    .flatMap(({ children }) => children)
    .reduce((acc, curr) => {
      const { key, type } = curr;

      if (type === ETypeInput.TextArea) {
        return acc;
      }

      if (type === ETypeInput.Switch) {
        acc[key] = Number(Math.random() > 0.5);
      }

      acc[key] = faker.number.int({ min: 0, max: 7 });

      return acc;
    }, {});

export const getAnswers = () =>
  Array.from({ length: 10 }, () => ({
    day: faker.date.past().toISOString(),
    fields: getFields(),
  }));
