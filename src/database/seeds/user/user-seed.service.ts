import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patient } from 'src/patients/entities/patient.entity';
import { User } from 'src/users/entities/user.entity';
import { RoleEnum } from 'src/roles/roles.enum';

import { doctors, patients, getAnswers } from './mocks';
import { StatusEnum } from 'src/statuses/statuses.enum';
import { Answer } from 'src/answers/entities/answer.entity';
import { answersByPatientOne } from './answers';

@Injectable()
export class UserSeedService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Patient)
    private patientsRepository: Repository<Patient>,
    @InjectRepository(Doctor)
    private doctorRepository: Repository<Doctor>,
    @InjectRepository(Answer)
    private answerRepository: Repository<Answer>,
  ) {}
  async generatePatient(doctor: Doctor, parentIndex: number) {
    const data: Patient[] = [];

    for (const {
      parentId,
      firstName,
      lastName,
      isLocal,
      ...rest
    } of patients) {
      if (parentId !== parentIndex) {
        continue;
      }

      const user = await this.usersRepository.save(
        this.usersRepository.create({
          ...rest,
          role: {
            id: RoleEnum.user,
            name: 'User',
          },
        }),
      );

      const patient = this.patientsRepository.create({
        firstName,
        lastName,
        isLocal,
        isPause: false,
        isDelete: false,
        user,
        status: {
          id: StatusEnum.active,
        },
      });
      const answers = [...getAnswers(), ...answersByPatientOne];

      await this.patientsRepository.save(patient);

      for (const answer of answers) {
        await this.answerRepository.save(
          this.answerRepository.create({ ...answer, patient }),
        );
      }

      data.push(patient);
    }

    return data;
  }

  async run() {
    const countAdmin = await this.usersRepository.count({
      where: {
        role: {
          id: RoleEnum.admin,
        },
      },
    });

    if (!countAdmin) {
      let currentIndex = 0;

      for (const { firstName, lastName, ...rest } of doctors) {
        const user = await this.usersRepository.save(
          this.usersRepository.create({
            ...rest,
            role: {
              id: RoleEnum.admin,
              name: 'Admin',
            },
          }),
        );

        const doctor = this.doctorRepository.create({
          firstName,
          lastName,
          user,
        });

        doctor.patients = await this.generatePatient(doctor, currentIndex);
        await this.doctorRepository.save(doctor);

        currentIndex += 1;
      }
    }
  }
}
