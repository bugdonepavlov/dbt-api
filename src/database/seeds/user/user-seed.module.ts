import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { UserSeedService } from './user-seed.service';
import { Patient } from 'src/patients/entities/patient.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Answer } from 'src/answers/entities/answer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Patient, Doctor, Answer])],
  providers: [UserSeedService],
  exports: [UserSeedService],
})
export class UserSeedModule {}
