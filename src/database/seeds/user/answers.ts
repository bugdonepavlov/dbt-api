export const answersByPatientOne = [
  {
    id: 37,
    createdAt: '2024-08-25 20:32:28.30678',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.stoptherapy\\":2,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":0,\\"emotions.anxiety\\":4,\\"emotions.envy\\":2,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":0,\\"emotions.sadness\\":3,\\"emotions.lost\\":0,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-25 23:29:48.742',
  },
  {
    id: 38,
    createdAt: '2024-08-27 20:05:12.631822',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.stoptherapy\\":1,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":1,\\"emotions.anxiety\\":2,\\"emotions.envy\\":2,\\"emotions.fear\\":0,\\"emotions.guilt\\":3,\\"emotions.happiness\\":1,\\"emotions.sadness\\":2,\\"emotions.lost\\":1,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-27 23:01:53.186',
  },
  {
    id: 39,
    createdAt: '2024-08-28 20:34:13.397068',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.stoptherapy\\":2,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.envy\\":1,\\"emotions.fear\\":0,\\"emotions.guilt\\":2,\\"emotions.happiness\\":1,\\"emotions.sadness\\":2,\\"emotions.lost\\":0,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-28 23:32:22.175',
  },
  {
    id: 40,
    createdAt: '2024-08-30 20:32:20.167821',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.stoptherapy\\":2,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":1,\\"emotions.envy\\":2,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":1,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":1,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-08-30 23:31:10.069',
  },
  {
    id: 41,
    createdAt: '2024-08-31 21:09:49.501358',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":2,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":3,\\"impulse.stoptherapy\\":4,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":4,\\"emotions.anxiety\\":2,\\"emotions.envy\\":4,\\"emotions.fear\\":3,\\"emotions.guilt\\":5,\\"emotions.happiness\\":1,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-01 00:07:09.547',
  },
  {
    id: 42,
    createdAt: '2024-09-01 17:43:24.814698',
    fields:
      '{\\"mood\\":\\"happy\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":5,\\"emotions.anxiety\\":5,\\"emotions.envy\\":0,\\"emotions.fear\\":2,\\"emotions.guilt\\":1,\\"emotions.happiness\\":5,\\"emotions.sadness\\":1,\\"emotions.lost\\":1,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":1,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-01 20:40:47.863',
  },
  {
    id: 63,
    createdAt: '2024-09-19 10:02:00.224359',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":4,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":4,\\"emotions.envy\\":2,\\"emotions.fear\\":1,\\"emotions.guilt\\":0,\\"emotions.happiness\\":1,\\"emotions.sadness\\":3,\\"emotions.lost\\":2,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-13 22:00:00',
  },
  {
    id: 71,
    createdAt: '2024-09-23 18:26:26.62163',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":4,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":4,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-23 21:25:40.875',
  },
  {
    id: 7,
    createdAt: '2024-07-23 16:44:50.430093',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":5,\\"impulse.drugs\\":3,\\"impulse.theft\\":3,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":3,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":true,\\"actions.drugs.about\\":\\"111\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-07-23 19:44:40.873',
  },
  {
    id: 8,
    createdAt: '2024-07-23 17:16:23.003688',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":5,\\"impulse.drugs\\":1,\\"impulse.theft\\":4,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":4,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":5,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":0,\\"emotions.sadness\\":3,\\"emotions.lost\\":4,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":true,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":3}',
    patientId: 1,
    day: '2024-07-23 20:15:43.069',
  },
  {
    id: 9,
    createdAt: '2024-07-23 17:16:53.297856',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":5,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":3,\\"impulse.drugs\\":3,\\"impulse.theft\\":3,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":5,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":1,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":1,\\"emotions.sadness\\":3,\\"emotions.lost\\":5,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":true,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":true,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":true,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-07-21 22:00:00',
  },
  {
    id: 10,
    createdAt: '2024-07-26 08:54:40.16718',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":0,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":2,\\"emotions.guilt\\":2,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":true,\\"actions.medications.about\\":\\"Венлафаксин 225 мг\\\\nКветиапин 100 мг\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-07-26 11:50:24.232',
  },
  {
    id: 11,
    createdAt: '2024-07-27 21:45:44.711858',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.theft\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":2,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":5,\\"emotions.envy\\":5,\\"emotions.fear\\":3,\\"emotions.guilt\\":5,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"Сладкое \\",\\"actions.lie\\":1,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-07-28 00:43:42.443',
  },
  {
    id: 12,
    createdAt: '2024-08-07 08:31:49.133548',
    fields:
      '{\\"mood\\":\\"happy\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":0,\\"emotions.envy\\":2,\\"emotions.fear\\":0,\\"emotions.guilt\\":2,\\"emotions.happiness\\":3,\\"emotions.sadness\\":1,\\"emotions.lost\\":1,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-06 01:00:00',
  },
  {
    id: 13,
    createdAt: '2024-08-07 20:33:58.490841',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.theft\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":1,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":2,\\"emotions.envy\\":3,\\"emotions.fear\\":2,\\"emotions.guilt\\":2,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-08-07 23:32:24.13',
  },
  {
    id: 14,
    createdAt: '2024-08-08 20:33:25.578861',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":0,\\"impulse.drugs\\":3,\\"impulse.theft\\":2,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":4,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":3,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":1,\\"emotions.envy\\":0,\\"emotions.fear\\":1,\\"emotions.guilt\\":1,\\"emotions.happiness\\":3,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-08-08 23:30:34.544',
  },
  {
    id: 15,
    createdAt: '2024-08-09 06:34:01.311878',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":4,\\"impulse.drugs\\":1,\\"impulse.theft\\":1,\\"impulse.screaming\\":4,\\"impulse.stoptherapy\\":4,\\"impulse.gambling\\":3,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":5,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":5,\\"emotions.envy\\":5,\\"emotions.fear\\":5,\\"emotions.guilt\\":5,\\"emotions.happiness\\":5,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":true,\\"actions.drugs.about\\":\\"хуй\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"по хуй\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-08-09 09:32:16.803',
  },
  {
    id: 16,
    createdAt: '2024-08-09 06:35:04.865656',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":5,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":4,\\"impulse.drugs\\":5,\\"impulse.theft\\":5,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":5,\\"impulse.gambling\\":5,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":5,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":5,\\"emotions.envy\\":5,\\"emotions.fear\\":5,\\"emotions.guilt\\":5,\\"emotions.happiness\\":5,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"хуй\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"по хуй\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":true,\\"actions.ganbling.about\\":\\"проебал хату\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-08 00:00:00',
  },
  {
    id: 17,
    createdAt: '2024-08-09 06:36:20.572651',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":3,\\"impulse.drugs\\":5,\\"impulse.theft\\":2,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":5,\\"impulse.gambling\\":5,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":5,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":5,\\"emotions.envy\\":5,\\"emotions.fear\\":5,\\"emotions.guilt\\":5,\\"emotions.happiness\\":5,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":true,\\"actions.alcohol.about\\":\\"нахмурился как свинья\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"хуй\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"по хуй\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"проебал хату\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-08-06 00:00:00',
  },
  {
    id: 18,
    createdAt: '2024-08-09 20:34:47.679887',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.theft\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":4,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":2,\\"emotions.fear\\":2,\\"emotions.guilt\\":4,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":4,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-09 23:30:40.286',
  },
  {
    id: 19,
    createdAt: '2024-08-10 21:16:27.095615',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.theft\\":2,\\"impulse.screaming\\":4,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":4,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":1,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":4,\\"emotions.envy\\":4,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":2,\\"emotions.sadness\\":4,\\"emotions.lost\\":4,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-11 00:14:19.16',
  },
  {
    id: 20,
    createdAt: '2024-08-12 19:05:50.743899',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.theft\\":0,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":4,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":4,\\"emotions.envy\\":4,\\"emotions.fear\\":2,\\"emotions.guilt\\":4,\\"emotions.happiness\\":2,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-08-11 01:00:00',
  },
  {
    id: 21,
    createdAt: '2024-08-12 19:07:30.92785',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":4,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":5,\\"impulse.theft\\":0,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":2,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":5,\\"emotions.envy\\":5,\\"emotions.fear\\":3,\\"emotions.guilt\\":5,\\"emotions.happiness\\":0,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-12 01:00:00',
  },
  {
    id: 22,
    createdAt: '2024-08-13 20:23:22.275111',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":0,\\"impulse.drugs\\":5,\\"impulse.theft\\":0,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":5,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":1,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":4,\\"emotions.envy\\":4,\\"emotions.fear\\":1,\\"emotions.guilt\\":5,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-13 23:21:22.209',
  },
  {
    id: 23,
    createdAt: '2024-08-14 20:27:20.605013',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":4,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":2,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":0,\\"emotions.envy\\":1,\\"emotions.fear\\":2,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":2,\\"emotions.lost\\":1,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":3}',
    patientId: 1,
    day: '2024-08-14 23:25:19.475',
  },
  {
    id: 24,
    createdAt: '2024-08-15 20:33:15.781383',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":1,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":0,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":1,\\"emotions.fear\\":1,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-15 23:30:48.591',
  },
  {
    id: 25,
    createdAt: '2024-08-16 16:10:51.25803',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":3,\\"impulse.drugs\\":3,\\"impulse.theft\\":2,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":3,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-08-16 19:09:32.472',
  },
  {
    id: 26,
    createdAt: '2024-08-16 20:04:32.118041',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.theft\\":1,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":1,\\"emotions.envy\\":0,\\"emotions.fear\\":1,\\"emotions.guilt\\":2,\\"emotions.happiness\\":3,\\"emotions.sadness\\":2,\\"emotions.lost\\":2,\\"emotions.shame\\":1,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-16 23:01:15.666',
  },
  {
    id: 27,
    createdAt: '2024-08-17 20:45:02.324552',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.theft\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":0,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":1,\\"emotions.guilt\\":3,\\"emotions.happiness\\":4,\\"emotions.sadness\\":0,\\"emotions.lost\\":1,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-08-17 23:43:10.079',
  },
  {
    id: 28,
    createdAt: '2024-08-18 12:27:21.698407',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":3,\\"impulse.drugs\\":3,\\"impulse.theft\\":3,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":3,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":true,\\"actions.alcohol.about\\":\\"sdasdasd\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"asdasd\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-18 15:26:59.333',
  },
  {
    id: 29,
    createdAt: '2024-08-18 20:28:16.184381',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":3,\\"impulse.drugs\\":5,\\"impulse.theft\\":4,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":4,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":3,\\"emotions.anger\\":5,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":2,\\"emotions.envy\\":4,\\"emotions.fear\\":4,\\"emotions.guilt\\":5,\\"emotions.happiness\\":1,\\"emotions.sadness\\":1,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-08-18 23:26:33.721',
  },
  {
    id: 30,
    createdAt: '2024-08-19 20:33:10.483703',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":5,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":2,\\"emotions.anxiety\\":5,\\"emotions.envy\\":5,\\"emotions.fear\\":3,\\"emotions.guilt\\":5,\\"emotions.happiness\\":0,\\"emotions.sadness\\":5,\\"emotions.lost\\":1,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":5,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-08-19 23:31:08.441',
  },
  {
    id: 31,
    createdAt: '2024-08-20 20:31:46.437149',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":2,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":3,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":2,\\"emotions.anxiety\\":3,\\"emotions.envy\\":4,\\"emotions.fear\\":3,\\"emotions.guilt\\":5,\\"emotions.happiness\\":2,\\"emotions.sadness\\":4,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":5,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-08-20 23:30:29.306',
  },
  {
    id: 32,
    createdAt: '2024-08-21 11:15:39.414934',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":1,\\"impulse.drugs\\":4,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":5,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":4,\\"emotions.anxiety\\":1,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":4,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":4,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-08-21 12:38:47.772',
  },
  {
    id: 33,
    createdAt: '2024-08-21 11:34:12.163196',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":3,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":2,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":2,\\"emotions.anxiety\\":1,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":1,\\"emotions.happiness\\":4,\\"emotions.sadness\\":4,\\"emotions.lost\\":4,\\"emotions.shame\\":5,\\"actions.alc\\":true,\\"actions.alcohol.about\\":\\"что-то\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":3}',
    patientId: 1,
    day: '2024-08-20 00:00:00',
  },
  {
    id: 34,
    createdAt: '2024-08-21 11:42:51.675852',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":2,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":1,\\"impulse.drugs\\":4,\\"impulse.stoptherapy\\":5,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":5,\\"emotions.happiness\\":5,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":true,\\"actions.alcohol.about\\":\\"ничего, но хотелось\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-08-19 00:00:00',
  },
  {
    id: 35,
    createdAt: '2024-08-22 20:32:01.621682',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":5,\\"impulse.stoptherapy\\":1,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":0,\\"emotions.anxiety\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":0,\\"emotions.guilt\\":3,\\"emotions.happiness\\":1,\\"emotions.sadness\\":3,\\"emotions.lost\\":1,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":3}',
    patientId: 1,
    day: '2024-08-22 23:30:47.511',
  },
  {
    id: 36,
    createdAt: '2024-08-24 20:31:41.392228',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.stoptherapy\\":0,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.envy\\":2,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":0,\\"emotions.sadness\\":2,\\"emotions.lost\\":0,\\"emotions.shame\\":1,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-08-24 23:30:30.264',
  },
  {
    id: 43,
    createdAt: '2024-09-04 20:32:07.106295',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":1,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":1,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":1,\\"emotions.envy\\":2,\\"emotions.fear\\":0,\\"emotions.guilt\\":2,\\"emotions.happiness\\":0,\\"emotions.sadness\\":1,\\"emotions.lost\\":2,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-04 23:30:33.985',
  },
  {
    id: 44,
    createdAt: '2024-09-05 07:46:59.769401',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":3,\\"impulse.drugs\\":1,\\"impulse.theft\\":4,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":3,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":4,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":4,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-04 13:10:58.52',
  },
  {
    id: 45,
    createdAt: '2024-09-06 20:24:14.670973',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":1,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":2,\\"emotions.envy\\":4,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":2,\\"emotions.sadness\\":4,\\"emotions.lost\\":1,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-06 23:22:25.835',
  },
  {
    id: 46,
    createdAt: '2024-09-07 20:26:13.269384',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":3,\\"impulse.theft\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":2,\\"emotions.envy\\":3,\\"emotions.fear\\":0,\\"emotions.guilt\\":3,\\"emotions.happiness\\":0,\\"emotions.sadness\\":4,\\"emotions.lost\\":0,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-07 23:24:46.222',
  },
  {
    id: 47,
    createdAt: '2024-09-08 20:32:22.783391',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":4,\\"impulse.drugs\\":5,\\"impulse.theft\\":1,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":1,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":2,\\"emotions.envy\\":4,\\"emotions.fear\\":2,\\"emotions.guilt\\":5,\\"emotions.happiness\\":0,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-08 23:30:35.302',
  },
  {
    id: 49,
    createdAt: '2024-09-11 19:11:25.10413',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":5,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":0,\\"impulse.drugs\\":5,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":5,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":5,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":5,\\"emotions.envy\\":0,\\"emotions.fear\\":5,\\"emotions.guilt\\":0,\\"emotions.happiness\\":0,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"побила себя кулаками по лицу и голове, небольшая шишка\\",\\"actions.screaming\\":true,\\"actions.screaming.about\\":\\"начала кричать от беспомощности когда била себя и в очередной раз рассталась\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"переела все что можно на свете \\",\\"actions.lie\\":0,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-11 20:32:06.138',
  },
  {
    id: 48,
    createdAt: '2024-09-11 06:52:16.451527',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":2,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":0,\\"impulse.drugs\\":5,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":1,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":5,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":0,\\"emotions.sadness\\":5,\\"emotions.lost\\":5,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"немного ударила себя по лицу кулаками с двух сторон, но несильно\\",\\"actions.screaming\\":true,\\"actions.screaming.about\\":\\"кричала на своего парня, из-за доты и рейтинга хд\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"хотела сначала заказать роллов и обожраться, сконтролировала себя, но все равно переела пельменей\\",\\"actions.lie\\":0,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-09 22:00:00',
  },
  {
    id: 50,
    createdAt: '2024-09-11 20:33:45.923252',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.theft\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":2,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":2,\\"emotions.guilt\\":2,\\"emotions.happiness\\":2,\\"emotions.sadness\\":0,\\"emotions.lost\\":0,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":7}',
    patientId: 1,
    day: '2024-09-11 23:29:44.189',
  },
  {
    id: 51,
    createdAt: '2024-09-12 21:09:12.237885',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":4,\\"emotions.fear\\":4,\\"emotions.guilt\\":4,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":3,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-13 00:07:20.425',
  },
  {
    id: 52,
    createdAt: '2024-09-13 20:24:04.592397',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":1,\\"emotions.fear\\":1,\\"emotions.guilt\\":3,\\"emotions.happiness\\":0,\\"emotions.sadness\\":4,\\"emotions.lost\\":0,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-13 23:22:32.481',
  },
  {
    id: 53,
    createdAt: '2024-09-14 21:59:02.124279',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":1,\\"emotions.envy\\":2,\\"emotions.fear\\":1,\\"emotions.guilt\\":3,\\"emotions.happiness\\":1,\\"emotions.sadness\\":3,\\"emotions.lost\\":0,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":3}',
    patientId: 1,
    day: '2024-09-15 00:57:25.351',
  },
  {
    id: 54,
    createdAt: '2024-09-15 20:43:00.495805',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":3,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":3,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":1,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":2,\\"emotions.guilt\\":1,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":3,\\"emotions.shame\\":0,\\"actions.alc\\":true,\\"actions.alcohol.about\\":\\"пол бутылки вина \\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-14 22:00:00',
  },
  {
    id: 55,
    createdAt: '2024-09-16 18:46:20.834149',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":4,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-16 21:45:09.375',
  },
  {
    id: 56,
    createdAt: '2024-09-16 20:32:13.378021',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":0,\\"impulse.drugs\\":1,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":3,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":1,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":4,\\"emotions.envy\\":4,\\"emotions.fear\\":3,\\"emotions.guilt\\":4,\\"emotions.happiness\\":0,\\"emotions.sadness\\":4,\\"emotions.lost\\":4,\\"emotions.shame\\":5,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-16 23:30:47.931',
  },
  {
    id: 57,
    createdAt: '2024-09-17 20:47:50.179525',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":1,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":0,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":3,\\"emotions.envy\\":4,\\"emotions.fear\\":0,\\"emotions.guilt\\":1,\\"emotions.happiness\\":0,\\"emotions.sadness\\":2,\\"emotions.lost\\":0,\\"emotions.shame\\":1,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-09-17 23:45:55.146',
  },
  {
    id: 58,
    createdAt: '2024-09-18 20:32:15.642138',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.theft\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":2,\\"emotions.envy\\":4,\\"emotions.fear\\":5,\\"emotions.guilt\\":3,\\"emotions.happiness\\":1,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-09-18 23:30:48.629',
  },
  {
    id: 59,
    createdAt: '2024-09-19 08:58:47.920421',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":5,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":true,\\"actions.alcohol.about\\":\\"на др мамы выпила вино пиво\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":true,\\"actions.screaming.about\\":\\"по пьяни немного поругались все ))\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-16 22:00:00',
  },
  {
    id: 60,
    createdAt: '2024-09-19 09:00:44.758634',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":3,\\"emotions.guilt\\":0,\\"emotions.happiness\\":2,\\"emotions.sadness\\":0,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-17 22:00:00',
  },
  {
    id: 61,
    createdAt: '2024-09-19 09:58:29.297942',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":5,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":5,\\"emotions.guilt\\":0,\\"emotions.happiness\\":0,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-11 22:00:00',
  },
  {
    id: 62,
    createdAt: '2024-09-19 09:59:40.008558',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":1,\\"emotions.guilt\\":0,\\"emotions.happiness\\":2,\\"emotions.sadness\\":2,\\"emotions.lost\\":1,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-12 22:00:00',
  },
  {
    id: 64,
    createdAt: '2024-09-19 18:46:48.903656',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":4,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":5,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":0,\\"emotions.shame\\":4,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"порезала немного ногу :(\\",\\"actions.screaming\\":true,\\"actions.screaming.about\\":\\"накричала сильно на макса\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"накупила булок и всё съела(((\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-19 21:45:05.969',
  },
  {
    id: 65,
    createdAt: '2024-09-19 21:38:44.07639',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":4,\\"emotions.envy\\":3,\\"emotions.fear\\":2,\\"emotions.guilt\\":2,\\"emotions.happiness\\":2,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-20 00:37:06.084',
  },
  {
    id: 66,
    createdAt: '2024-09-21 11:28:49.945025',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":1,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":2,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":2,\\"emotions.happiness\\":4,\\"emotions.sadness\\":3,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":true,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":true,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-19 22:00:00',
  },
  {
    id: 67,
    createdAt: '2024-09-21 22:41:14.917655',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":0,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":0,\\"emotions.envy\\":3,\\"emotions.fear\\":0,\\"emotions.guilt\\":3,\\"emotions.happiness\\":1,\\"emotions.sadness\\":2,\\"emotions.lost\\":0,\\"emotions.shame\\":1,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-21 01:00:00',
  },
  {
    id: 68,
    createdAt: '2024-09-22 18:16:45.236828',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":1,\\"emotions.disgust\\":0,\\"emotions.envy\\":3,\\"emotions.fear\\":0,\\"emotions.guilt\\":3,\\"emotions.happiness\\":4,\\"emotions.sadness\\":3,\\"emotions.lost\\":0,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-20 22:00:00',
  },
  {
    id: 69,
    createdAt: '2024-09-22 18:17:09.23012',
    fields:
      '{\\"mood\\":\\"happy\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":5,\\"emotions.sadness\\":0,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-21 22:00:00',
  },
  {
    id: 70,
    createdAt: '2024-09-22 20:33:02.964285',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":2,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":1,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":2,\\"emotions.anger\\":0,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":3,\\"emotions.fear\\":0,\\"emotions.guilt\\":3,\\"emotions.happiness\\":0,\\"emotions.sadness\\":4,\\"emotions.lost\\":0,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":4,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-22 23:31:04.026',
  },
  {
    id: 72,
    createdAt: '2024-09-24 21:02:30.360182',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":1,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":0,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":1,\\"emotions.envy\\":4,\\"emotions.fear\\":1,\\"emotions.guilt\\":2,\\"emotions.happiness\\":1,\\"emotions.sadness\\":3,\\"emotions.lost\\":4,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-09-25 00:00:36.479',
  },
  {
    id: 73,
    createdAt: '2024-09-25 20:32:59.805537',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":0,\\"impulse.drugs\\":3,\\"impulse.theft\\":0,\\"impulse.screaming\\":4,\\"impulse.stoptherapy\\":2,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":4,\\"emotions.anger\\":2,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":4,\\"emotions.envy\\":2,\\"emotions.fear\\":1,\\"emotions.guilt\\":2,\\"emotions.happiness\\":0,\\"emotions.sadness\\":4,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":2,\\"actions.skills\\":4}',
    patientId: 1,
    day: '2024-09-25 23:31:16.423',
  },
  {
    id: 74,
    createdAt: '2024-09-26 08:07:19.40158',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":2,\\"emotions.sadness\\":3,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-23 22:00:00',
  },
  {
    id: 75,
    createdAt: '2024-09-26 08:07:54.193906',
    fields:
      '{\\"mood\\":\\"smile\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":0,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":2,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":4,\\"emotions.sadness\\":2,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-24 22:00:00',
  },
  {
    id: 78,
    createdAt: '2024-09-27 18:09:25.921385',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":1,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":1,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":1,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":3,\\"emotions.sadness\\":1,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":0}',
    patientId: 1,
    day: '2024-09-26 22:00:00',
  },
  {
    id: 76,
    createdAt: '2024-09-27 08:59:53.489613',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":0,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.theft\\":0,\\"impulse.screaming\\":0,\\"impulse.stoptherapy\\":0,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":3,\\"emotions.emsuffering\\":5,\\"emotions.phsuffering\\":5,\\"emotions.anger\\":3,\\"emotions.anxiety\\":3,\\"emotions.disgust\\":3,\\"emotions.envy\\":3,\\"emotions.fear\\":3,\\"emotions.guilt\\":3,\\"emotions.happiness\\":3,\\"emotions.sadness\\":3,\\"emotions.lost\\":3,\\"emotions.shame\\":3,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":true,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":true,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":true,\\"actions.medications.about\\":\\"asdasd\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":3,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-27 11:58:05.51',
  },
  {
    id: 77,
    createdAt: '2024-09-27 18:08:52.033317',
    fields:
      '{\\"mood\\":\\"sad\\",\\"impulse.suicide\\":2,\\"impulse.slfhrm\\":0,\\"impulse.alc\\":0,\\"impulse.drugs\\":0,\\"impulse.screaming\\":2,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":2,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":2,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":0,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":0,\\"emotions.happiness\\":3,\\"emotions.sadness\\":2,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":true,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":2}',
    patientId: 1,
    day: '2024-09-25 22:00:00',
  },
  {
    id: 79,
    createdAt: '2024-09-28 20:11:16.221122',
    fields:
      '{\\"mood\\":\\"neutral\\",\\"impulse.suicide\\":2,\\"impulse.slfhrm\\":3,\\"impulse.alc\\":0,\\"impulse.drugs\\":2,\\"impulse.theft\\":0,\\"impulse.screaming\\":3,\\"impulse.stoptherapy\\":1,\\"impulse.gambling\\":0,\\"impulse.bingeeating\\":4,\\"emotions.emsuffering\\":3,\\"emotions.phsuffering\\":1,\\"emotions.anger\\":3,\\"emotions.anxiety\\":4,\\"emotions.disgust\\":3,\\"emotions.envy\\":4,\\"emotions.fear\\":2,\\"emotions.guilt\\":4,\\"emotions.happiness\\":0,\\"emotions.sadness\\":5,\\"emotions.lost\\":4,\\"emotions.shame\\":2,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.theft\\":false,\\"actions.theft.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.ganbling\\":false,\\"actions.ganbling.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":5,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-28 23:09:33.879',
  },
  {
    id: 80,
    createdAt: '2024-09-29 10:17:03.175699',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":4,\\"impulse.slfhrm\\":4,\\"impulse.alc\\":0,\\"impulse.drugs\\":4,\\"impulse.screaming\\":4,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":4,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":4,\\"emotions.anxiety\\":2,\\"emotions.disgust\\":2,\\"emotions.envy\\":0,\\"emotions.fear\\":1,\\"emotions.guilt\\":0,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":1,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":5}',
    patientId: 1,
    day: '2024-09-27 22:00:00',
  },
  {
    id: 81,
    createdAt: '2024-09-29 18:55:35.165768',
    fields:
      '{\\"mood\\":\\"empty\\",\\"impulse.suicide\\":5,\\"impulse.slfhrm\\":5,\\"impulse.alc\\":0,\\"impulse.drugs\\":5,\\"impulse.screaming\\":5,\\"impulse.stoptherapy\\":0,\\"impulse.bingeeating\\":0,\\"emotions.emsuffering\\":4,\\"emotions.phsuffering\\":0,\\"emotions.anger\\":4,\\"emotions.anxiety\\":0,\\"emotions.disgust\\":2,\\"emotions.envy\\":0,\\"emotions.fear\\":0,\\"emotions.guilt\\":1,\\"emotions.happiness\\":1,\\"emotions.sadness\\":4,\\"emotions.lost\\":0,\\"emotions.shame\\":0,\\"actions.alc\\":false,\\"actions.alcohol.about\\":\\"\\",\\"actions.drugs\\":false,\\"actions.drugs.about\\":\\"\\",\\"actions.harm\\":false,\\"actions.harm.about\\":\\"\\",\\"actions.screaming\\":false,\\"actions.screaming.about\\":\\"\\",\\"actions.medications\\":false,\\"actions.medications.about\\":\\"\\",\\"actions.prn\\":false,\\"actions.prn.about\\":\\"\\",\\"actions.bingeeating\\":false,\\"actions.bingeeating.about\\":\\"\\",\\"actions.lie\\":0,\\"actions.skills\\":1}',
    patientId: 1,
    day: '2024-09-29 21:54:02.59',
  },
];
