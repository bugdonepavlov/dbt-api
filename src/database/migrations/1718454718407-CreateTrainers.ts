import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTrainers1718454718407 implements MigrationInterface {
  name = 'CreateTrainers1718454718407';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patient" RENAME COLUMN "publicPassword" TO "trainers"`,
    );
    await queryRunner.query(
      `CREATE TYPE "public"."trainer_type_enum" AS ENUM('coach')`,
    );
    await queryRunner.query(
      `CREATE TABLE "trainer" ("id" SERIAL NOT NULL, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "middleName" character varying, "phone" character varying NOT NULL, "extraPhone" character varying, "type" "public"."trainer_type_enum" NOT NULL DEFAULT 'coach', "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_8dfa741df6d52a0da8ad93f0c7e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "trainers_to_doctors" ("trainerId" integer NOT NULL, "doctorId" integer NOT NULL, CONSTRAINT "PK_797ede46e7db92449cdb29d75af" PRIMARY KEY ("trainerId", "doctorId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_f92b2c8c7893e044477abbd9d8" ON "trainers_to_doctors" ("trainerId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_e5cb4c06a1b3976c73dcad92c8" ON "trainers_to_doctors" ("doctorId") `,
    );
    await queryRunner.query(`ALTER TABLE "patient" DROP COLUMN "trainers"`);
    await queryRunner.query(
      `ALTER TABLE "patient" ADD "trainers" integer array NOT NULL DEFAULT '{}'`,
    );
    await queryRunner.query(
      `ALTER TABLE "trainers_to_doctors" ADD CONSTRAINT "FK_f92b2c8c7893e044477abbd9d8b" FOREIGN KEY ("trainerId") REFERENCES "trainer"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "trainers_to_doctors" ADD CONSTRAINT "FK_e5cb4c06a1b3976c73dcad92c88" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(
      `ALTER TABLE "trainers_to_doctors" DROP CONSTRAINT "FK_e5cb4c06a1b3976c73dcad92c88"`,
    );
    await queryRunner.query(
      `ALTER TABLE "trainers_to_doctors" DROP CONSTRAINT "FK_f92b2c8c7893e044477abbd9d8b"`,
    );
    await queryRunner.query(`ALTER TABLE "patient" DROP COLUMN "trainers"`);
    await queryRunner.query(
      `ALTER TABLE "patient" ADD "trainers" character varying`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_e5cb4c06a1b3976c73dcad92c8"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_f92b2c8c7893e044477abbd9d8"`,
    );
    await queryRunner.query(`DROP TABLE "trainers_to_doctors"`);
    await queryRunner.query(`DROP TABLE "trainer"`);
    await queryRunner.query(`DROP TYPE "public"."trainer_type_enum"`);
    await queryRunner.query(
      `ALTER TABLE "patient" RENAME COLUMN "trainers" TO "publicPassword"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
