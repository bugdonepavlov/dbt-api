import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTableTelegramBotDB1724418137793
  implements MigrationInterface
{
  name = 'CreateTableTelegramBotDB1724418137793';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(
      `CREATE SEQUENCE IF NOT EXISTS "telegram_user_id_seq" OWNED BY "telegram_user"."id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "telegram_user" ALTER COLUMN "id" SET DEFAULT nextval('"telegram_user_id_seq"')`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(
      `ALTER TABLE "telegram_user" ALTER COLUMN "id" DROP DEFAULT`,
    );
    await queryRunner.query(`DROP SEQUENCE "telegram_user_id_seq"`);
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
