import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTableTelegramBot1724239802151 implements MigrationInterface {
  name = 'CreateTableTelegramBot1724239802151';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(
      `CREATE TABLE "telegram_user" ("id" integer NOT NULL, "timezone" character varying NOT NULL, "time" character varying NOT NULL, "userId" integer NOT NULL, CONSTRAINT "PK_8e00b1def3edd3510248136f820" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(`DROP TABLE "telegram_user"`);
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
