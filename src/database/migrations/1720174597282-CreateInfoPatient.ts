import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateInfoPatient1720174597282 implements MigrationInterface {
  name = 'CreateInfoPatient1720174597282';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patient" ADD "middleName" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "patient" ADD "phone" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "patient" ADD "telegram" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(`ALTER TABLE "patient" DROP COLUMN "telegram"`);
    await queryRunner.query(`ALTER TABLE "patient" DROP COLUMN "phone"`);
    await queryRunner.query(`ALTER TABLE "patient" DROP COLUMN "middleName"`);
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
