import { MigrationInterface, QueryRunner } from 'typeorm';

export class Table1231721752910720 implements MigrationInterface {
  name = 'Table1231721752910720';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(`ALTER TABLE "answer" DROP COLUMN "day"`);
    await queryRunner.query(
      `ALTER TABLE "answer" ADD "day" TIMESTAMP NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" DROP CONSTRAINT "FK_a0af3e0460e1f373902f13468ca"`,
    );
    await queryRunner.query(`ALTER TABLE "answer" DROP COLUMN "day"`);
    await queryRunner.query(
      `ALTER TABLE "answer" ADD "day" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_a0af3e0460e1f373902f13468ca" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "patients_to_doctors" ADD CONSTRAINT "FK_53d3678ac544b53fd6b90178ce5" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
