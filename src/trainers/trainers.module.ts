import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IsExist } from 'src/utils/validators/is-exists.validator';
import { IsNotExist } from 'src/utils/validators/is-not-exists.validator';
import { Trainer } from './entities/trainer.entity';
import { TrainersService } from './trainers.service';
import { TrainersController } from './trainers.controller';
import { DoctorsModule } from 'src/doctors/doctors.module';

@Module({
  imports: [TypeOrmModule.forFeature([Trainer]), DoctorsModule],
  controllers: [TrainersController],
  providers: [IsExist, IsNotExist, TrainersService],
  exports: [TrainersService],
})
export class TrainersModule {}
