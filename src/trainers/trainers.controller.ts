import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  HttpStatus,
  HttpCode,
  SerializeOptions,
  UseInterceptors,
  Request,
  Put,
  Delete,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { TrainersService } from './trainers.service';
import { CreateTrainertDto } from './dto/create-trainer.dto';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';

@ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiTags('trainers')
@Controller({
  path: 'trainers',
  version: '1',
})
@UseInterceptors(TransformationInterceptor)
export class TrainersController {
  constructor(private readonly trainersService: TrainersService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  findAll(@Request() request) {
    return this.trainersService.findAllByDoctor(+request.user.id);
  }

  @SerializeOptions({
    groups: ['admin'],
  })
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  create(
    @Body() createTrainerDto: CreateTrainertDto,
    @Request() request,
  ): Promise<unknown> {
    return this.trainersService.create(createTrainerDto, +request.user.id);
  }

  @SerializeOptions({
    groups: ['admin'],
  })
  @Put(':id')
  @HttpCode(HttpStatus.OK)
  update(@Param('id') id: number, @Body() updateTrainerDto: CreateTrainertDto) {
    return this.trainersService.update(id, updateTrainerDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  remove(@Param('id') id: number) {
    return this.trainersService.remove(+id);
  }
}
