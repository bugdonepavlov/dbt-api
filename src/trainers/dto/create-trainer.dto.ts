import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateTrainertDto {
  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({ example: 'Doe' })
  @IsOptional()
  middleName: string;

  @ApiProperty({ example: '+79999999999' })
  @IsNotEmpty()
  phone: string;

  @ApiProperty({ example: '+79999999999' })
  @IsOptional()
  extraPhone: string;
}
