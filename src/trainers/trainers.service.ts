import { InjectRepository } from '@nestjs/typeorm';
import { Trainer } from './entities/trainer.entity';
import { Repository } from 'typeorm';
import { EntityCondition } from 'src/utils/types/entity-condition.type';
import { CreateTrainertDto } from './dto/create-trainer.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import { DoctorsService } from 'src/doctors/doctors.service';

export class TrainersService {
  constructor(
    @InjectRepository(Trainer)
    private trainerRepository: Repository<Trainer>,
    private doctorServise: DoctorsService,
  ) {}

  findOne(fields: EntityCondition<Trainer>) {
    return this.trainerRepository.findOne({
      where: fields,
    });
  }

  async findAllByDoctor(userId: number) {
    const doctor = await this.doctorServise.findOne({
      user: {
        id: userId,
      },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          errors: {
            hash: `Такой юзер уже есть`,
          },
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const trainers = await this.trainerRepository.find({
      where: {
        doctor: {
          id: doctor.id,
        },
      },
      order: {
        firstName: 'ASC',
      },
    });

    return trainers.map(
      ({ firstName, lastName, middleName, phone, id, extraPhone }) => ({
        firstName,
        lastName,
        middleName,
        phone,
        extraPhone,
        id,
      }),
    );
  }

  async create(fields: CreateTrainertDto, userId: number) {
    const doctor = await this.doctorServise.findOne({
      user: {
        id: userId,
      },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          errors: {
            hash: `Такой юзер уже есть`,
          },
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const trainer = await this.trainerRepository.findOne({
      where: {
        phone: fields.phone,
        doctor: {
          id: doctor.id,
        },
      },
    });

    if (trainer) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          errors: {
            hash: `Такой юзер уже есть`,
          },
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const createdTrainer = this.trainerRepository.create({
      ...fields,
      doctor,
    });

    const { id, firstName, lastName, middleName, phone, extraPhone } =
      await this.trainerRepository.save(createdTrainer);

    return {
      firstName,
      lastName,
      middleName,
      id,
      phone,
      extraPhone,
    };
  }

  async update(id: number, payload: CreateTrainertDto) {
    const trainer = await this.findOne({ id });

    if (!trainer) {
      throw new HttpException(
        {
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          errors: {
            hash: `notFound`,
          },
        },
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    await this.trainerRepository.update(id, payload);

    return { ok: true };
  }

  async remove(trainerId: number) {
    await this.trainerRepository.delete(trainerId);

    return { ok: true };
  }
}
