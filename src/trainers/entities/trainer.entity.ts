import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EntityHelper } from 'src/utils/entity-helper';
import { ETypeTrainer } from '../interfaces';
import { Doctor } from 'src/doctors/entities/doctor.entity';

@Entity()
export class Trainer extends EntityHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  middleName: string;

  @Column()
  phone: string;

  @Column({ nullable: true })
  extraPhone: string;

  @Column({
    type: 'enum',
    enum: ETypeTrainer,
    default: ETypeTrainer.Coach,
  })
  type: ETypeTrainer;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => Doctor, (doctor) => doctor.trainers)
  doctor: Doctor;
}
