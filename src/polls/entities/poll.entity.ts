import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EntityHelper } from 'src/utils/entity-helper';

@Entity()
export class Poll extends EntityHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'json',
    transformer: {
      to(value: string[]): string {
        return JSON.stringify(value);
      },
      from(value: string): string[] {
        return JSON.parse(value);
      },
    },
  })
  items: string[];

  @Column({
    type: 'json',
    transformer: {
      to(value: string[]): string {
        return JSON.stringify(value);
      },
      from(value: string): string[] {
        return JSON.parse(value);
      },
    },
  })
  disabled: string[];

  @Column({ type: 'numeric', nullable: true, default: null })
  patient: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
