export interface IPollsResponse {
  disabledFields: string[];
}

export interface IPollListResponse {
  disabled: string[];
  items: IPoll[];
  uniqueKeys: string[];
}

export interface IRadioListItem {
  title: string;
  val: number;
}

export interface IPollItem {
  title: string;
  shortTitle?: string;
  key: string;
  description: string;
  type: ETypeInput;
  list?: IRadioListItem[];
  max?: number;
  about?: {
    placeholder: string;
    type: ETypeInput;
    key: string;
  };
}

export interface IPoll {
  title: string;
  shortTitle?: string;
  key: string;
  children: IPollItem[];
}

export enum ETypeInput {
  Slider = 'slider',
  Switch = 'switch',
  TextArea = 'textArea',
  Radio = 'radio',
}
