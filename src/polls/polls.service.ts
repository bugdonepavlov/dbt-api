import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Poll } from './entities/poll.entity';
import { pollsItems, pollsItemsChildren } from './utils';
import { IPoll, IPollItem, IPollListResponse } from './polls.interfaces';

@Injectable()
export class PollsService {
  constructor(
    @InjectRepository(Poll)
    private pollsRepository: Repository<Poll>,
  ) {}

  async findByPatient(patientId: string): Promise<IPoll[]> {
    const pollByUser = await this.pollsRepository.findOne({
      where: { patient: +patientId },
    });

    if (!pollByUser || pollByUser?.items.length === 0) {
      return pollsItems;
    }

    const data: IPoll[] = [];
    const { disabled } = pollByUser;

    for (const { children, title, key } of pollsItems) {
      const filtredChildren = children.filter(
        ({ key }: IPollItem) => !disabled.includes(key),
      );

      if (filtredChildren.length === 0) {
        continue;
      }

      data.push({ title, key, children: filtredChildren });
    }

    return data;
  }

  async findPolls(patientId: string): Promise<IPollListResponse> {
    const pollByUser = await this.pollsRepository.findOne({
      where: { patient: +patientId },
      order: { createdAt: 'DESC' },
    });
    const polls = await this.pollsRepository.find({
      where: { patient: +patientId },
    });

    const keys = polls?.flatMap(({ items }) => items);
    const uniqueKeys = [...new Set(keys).values()];

    return {
      items: pollsItems,
      disabled: pollByUser?.disabled || [],
      uniqueKeys,
    };
  }

  saveDisabledPolls(disabledFields: string[], patientId: number) {
    const items = pollsItemsChildren.filter(
      (key: string) => !disabledFields.includes(key),
    );
    const newPoll = this.pollsRepository.create({
      patient: +patientId,
      items,
      disabled: disabledFields,
    });

    return this.pollsRepository.save(newPoll);
  }
}
