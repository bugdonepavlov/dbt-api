import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  SerializeOptions,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PollsService } from './polls.service';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';

@ApiTags('Polls')
@Controller({
  path: 'polls',
  version: '1',
})
@UseInterceptors(TransformationInterceptor)
export class PollsController {
  constructor(private service: PollsService) {}

  @SerializeOptions({
    groups: ['patient'],
  })
  @Get(':id/list')
  @HttpCode(HttpStatus.OK)
  findAll(@Param('id') id: string) {
    return this.service.findByPatient(id);
  }

  @SerializeOptions({
    groups: ['admin'],
  })
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findAllDisabled(@Param('id') id: string) {
    return this.service.findPolls(id);
  }

  @SerializeOptions({
    groups: ['admin'],
  })
  @Post(':id')
  @HttpCode(HttpStatus.OK)
  saveAllDisabled(@Param('id') id: string, @Body() disabled: string[]) {
    return this.service.saveDisabledPolls(disabled, +id);
  }
}
