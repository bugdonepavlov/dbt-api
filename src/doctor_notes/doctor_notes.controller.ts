import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  Put,
  Request,
  SerializeOptions,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';
import { AuthGuard } from '@nestjs/passport';
import { DoctorsNotesService } from './doctor_notes.service';

@ApiTags('Doctor_notes')
@Controller({
  path: 'doctor-notes',
  version: '1',
})
@SerializeOptions({
  groups: ['admin'],
})
@UseInterceptors(TransformationInterceptor)
export class DoctorNotesController {
  constructor(private service: DoctorsNotesService) {}

  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  pausePatient(@Param('id') id: string, @Request() request, @Body() body) {
    const params = {
      doctorId: request.user.id as number,
      patientId: +id,
      ...body,
    };

    return this.service.update(params);
  }
}
