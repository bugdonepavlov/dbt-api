import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { NullableType } from 'src/utils/types/nullable.type';

import { DoctorNote } from './entities/doctor_note.entity';
import { CreateNoteDto } from './dto/create-note.dto';

@Injectable()
export class DoctorsNotesService {
  constructor(
    @InjectRepository(DoctorNote)
    private doctorsNotesRepository: Repository<DoctorNote>,
  ) {}

  save(createNoteDto: CreateNoteDto) {
    return this.doctorsNotesRepository.save(
      this.doctorsNotesRepository.create(createNoteDto),
    );
  }

  update({ doctorId, patientId, note }) {
    return this.doctorsNotesRepository.update(
      { doctor: doctorId, patient: patientId },
      {
        note: note,
      },
    );
  }

  findOne(
    doctorId: number,
    patientId: number,
  ): Promise<NullableType<DoctorNote>> {
    return this.doctorsNotesRepository.findOne({
      where: {
        doctor: {
          id: doctorId,
        },
        patient: {
          id: patientId,
        },
      },
    });
  }

  find(): Promise<NullableType<DoctorNote[]>> {
    return this.doctorsNotesRepository.find();
  }
}
