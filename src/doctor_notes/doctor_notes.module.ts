import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DoctorNote } from './entities/doctor_note.entity';
import { DoctorsNotesService } from './doctor_notes.service';
import { DoctorNotesController } from './doctor_notes.controller';

@Module({
  imports: [TypeOrmModule.forFeature([DoctorNote])],
  controllers: [DoctorNotesController],
  providers: [DoctorsNotesService],
  exports: [DoctorsNotesService],
})
export class DoctorsNotesModule {}
