import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, Validate } from 'class-validator';
import { IsNotExist } from 'src/utils/validators/is-not-exists.validator';
import { Patient } from 'src/patients/entities/patient.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';

export class CreateNoteDto {
  @IsNotEmpty()
  @Validate(IsNotExist, ['Patient'], {
    message: 'patientAlreadyExists',
  })
  patient: Patient;

  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  doctor: Doctor;

  @ApiProperty({ example: 'Играет в call of duty' })
  @IsOptional()
  note: string;
}
