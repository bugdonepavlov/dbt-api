import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EntityHelper } from 'src/utils/entity-helper';
import { User } from 'src/users/entities/user.entity';
import { Patient } from 'src/patients/entities/patient.entity';
import { Trainer } from 'src/trainers/entities/trainer.entity';

@Entity()
export class Doctor extends EntityHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @ManyToOne(() => User, {
    eager: true,
  })
  user: User;

  @Column({ nullable: true })
  phone: string;

  @ManyToMany(() => Patient, (patient) => patient.doctors)
  @JoinTable({ name: 'patients_to_doctors' })
  patients: Patient[];

  @OneToMany(() => Trainer, (trainer) => trainer.doctor)
  trainers: Trainer[];
}
