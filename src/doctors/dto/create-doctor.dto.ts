import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateDoctorDto {
  @ApiProperty({ example: 'John' })
  @IsNotEmpty()
  firstName: string;

  @ApiProperty({ example: 'Doe' })
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  userId: number;

  @ApiProperty({ example: '+7 999 999 99 99' })
  @IsOptional()
  phone: string;
}
