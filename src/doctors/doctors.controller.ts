import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Put,
  Request,
  SerializeOptions,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DoctorsService } from './doctors.service';
import { AuthGuard } from '@nestjs/passport';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';
import { UpdateDoctorDto } from './dto/update-doctor.dto';

@ApiTags('Doctors')
@Controller({
  path: 'doctors',
  version: '1',
})
@UseInterceptors(TransformationInterceptor)
export class DoctorsController {
  constructor(private service: DoctorsService) {}

  @SerializeOptions({
    groups: ['admin'],
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('info')
  @HttpCode(HttpStatus.OK)
  findOneByUser(@Request() request) {
    return this.service.findOneByUser(request.user.id);
  }

  @SerializeOptions({
    groups: ['admin'],
  })
  @Put('info/:id')
  @HttpCode(HttpStatus.OK)
  updateInfoDoctor(
    @Param('id') doctorId: string,
    @Body() updateDoctorDto: UpdateDoctorDto,
  ) {
    return this.service.update(+doctorId, updateDoctorDto);
  }

  @SerializeOptions({
    groups: ['admin'],
  })
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findAll(@Param('id') id: string) {
    return this.service.findOne({ id: +id });
  }
}
