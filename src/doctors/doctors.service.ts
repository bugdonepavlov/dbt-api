import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Doctor } from './entities/doctor.entity';
import { EntityCondition } from 'src/utils/types/entity-condition.type';
import { NullableType } from 'src/utils/types/nullable.type';
import { CreateDoctorDto } from './dto/create-doctor.dto';
import { UpdateDoctorDto } from './dto/update-doctor.dto';

@Injectable()
export class DoctorsService {
  constructor(
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
  ) {}

  findOne(fields: EntityCondition<Doctor>): Promise<NullableType<Doctor>> {
    return this.doctorsRepository.findOne({ where: fields });
  }

  async findOneByUser(userId: number) {
    const doctor = await this.doctorsRepository.findOne({
      where: {
        user: {
          id: userId,
        },
      },
      relations: {
        user: true,
      },
    });

    if (!doctor) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          errors: {
            email: 'doctorNotFound',
          },
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const { id, phone, firstName, lastName } = doctor;

    return { id, phone, firstName, lastName };
  }

  create(createDoctorDto: CreateDoctorDto) {
    const { userId, ...rest } = createDoctorDto;

    return this.doctorsRepository.save(
      this.doctorsRepository.create({
        ...rest,
        user: {
          id: userId,
        },
      }),
    );
  }

  async update(doctorId: number, updateDoctorDto: UpdateDoctorDto) {
    return this.doctorsRepository.update(doctorId, updateDoctorDto);
  }
}
