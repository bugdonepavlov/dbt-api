import generator from 'generate-password';
import crypto from 'crypto';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';

export const generateHash = () => {
  return crypto
    .createHash('sha256')
    .update(randomStringGenerator())
    .digest('hex');
};

export const generatePassword = () =>
  generator.generate({
    length: 10,
    numbers: true,
  });
