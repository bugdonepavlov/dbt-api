import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MailData } from './interfaces/mail-data.interface';
import { AllConfigType } from 'src/config/config.type';
import { MaybeType } from '../utils/types/maybe.type';
import { MailerService } from 'src/mailer/mailer.service';
import path from 'path';

@Injectable()
export class MailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService<AllConfigType>,
  ) {}

  async patientActivate(
    mailData: MailData<{ hash: string; password: string }>,
  ): Promise<void> {
    await this.mailerService.sendMail({
      to: mailData.to,
      subject: 'Вам дали доступ на платформу',
      text: `${this.configService.get('app.frontendDomain', {
        infer: true,
      })}/confirm-email?hash=${
        mailData.data.hash
      } Вам дали доступ на платформу`,
      templatePath: path.join(
        this.configService.getOrThrow('app.workingDirectory', {
          infer: true,
        }),
        'src',
        'mail',
        'mail-templates',
        'activation.hbs',
      ),
      context: {
        title: 'Вам дали доступ на платформу',
        url: `${this.configService.get('app.frontendDomain', {
          infer: true,
        })}/auth/login?hash=${mailData.data.hash}`,
        actionTitle: 'Вам дали доступ на платформу',
        app_name: this.configService.get('app.name', { infer: true }),
        text1: 'Введите данные',
        text2: mailData.to,
        text3: mailData.data.password,
      },
    });
  }

  async userSignUp(mailData: MailData<{ hash: string }>): Promise<void> {
    const emailConfirmTitle: MaybeType<string> = 'Подтверждение эмайла';
    const text1: MaybeType<string> = 'Здравствуйте!';
    const text2: MaybeType<string> =
      'Нажмите на кнопку, чтоб подтвердить e-mail';
    let text3: MaybeType<string>;

    await this.mailerService.sendMail({
      to: mailData.to,
      subject: emailConfirmTitle,
      text: `https://admin.dbtcard.com/auth/confirm-email?hash=${mailData.data.hash} ${emailConfirmTitle}`,
      templatePath: path.join(
        this.configService.getOrThrow('app.workingDirectory', {
          infer: true,
        }),
        'src',
        'mail',
        'mail-templates',
        'activation.hbs',
      ),
      context: {
        title: emailConfirmTitle,
        url: `https://admin.dbtcard.com/auth/confirm-email?hash=${mailData.data.hash}`,
        actionTitle: emailConfirmTitle,
        app_name: this.configService.get('app.name', { infer: true }),
        text1,
        text2,
        text3,
      },
    });
  }

  async forgotPassword(mailData: MailData<{ hash: string }>): Promise<void> {
    const resetPasswordTitle: MaybeType<string> = 'Авторизоваться';
    let text1: MaybeType<string>;
    let text2: MaybeType<string>;
    let text3: MaybeType<string>;
    let text4: MaybeType<string>;

    await this.mailerService.sendMail({
      to: mailData.to,
      subject: resetPasswordTitle,
      text: `${this.configService.get('app.frontendDomain', {
        infer: true,
      })}/auth/reset?hash=${mailData.data.hash} ${resetPasswordTitle}`,
      templatePath: path.join(
        this.configService.getOrThrow('app.workingDirectory', {
          infer: true,
        }),
        'src',
        'mail',
        'mail-templates',
        'reset-password.hbs',
      ),
      context: {
        title: resetPasswordTitle,
        url: `${this.configService.get('app.frontendDomain', {
          infer: true,
        })}/auth/reset?hash=${mailData.data.hash}`,
        actionTitle: resetPasswordTitle,
        app_name: this.configService.get('app.name', {
          infer: true,
        }),
        text1,
        text2,
        text3,
        text4,
      },
    });
  }
}
