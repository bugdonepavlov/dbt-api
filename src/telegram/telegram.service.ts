import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TelegramUser } from './telegram-user.entity';

@Injectable()
export class TelegramBotService {
  private readonly logger = new Logger(TelegramBotService.name);

  constructor(
    @InjectRepository(TelegramUser)
    private telegramUsersRepository: Repository<TelegramUser>,
  ) {}

  findOneByUserId(userId: number) {
    return this.telegramUsersRepository.findOne({
      where: { userId },
    });
  }

  async saveTelegramUser(userId: number) {
    if (userId) {
      return this.telegramUsersRepository.save(
        this.telegramUsersRepository.create({
          userId,
          timezone: '',
          time: '',
        }),
      );
    }
  }

  async updateTelegramUser(
    userId: number,
    props: { time: string; timezone?: string },
  ) {
    try {
      const user = await this.findOneByUserId(userId);

      if (!user) {
        return this.telegramUsersRepository.save(
          this.telegramUsersRepository.create({
            userId,
            timezone: '',
            time: props.time,
          }),
        );
      } else {
        const params = { ...user, ...props };

        return this.telegramUsersRepository.update(
          { id: user.id },
          { time: params.time, timezone: params.timezone },
        );
      }
    } catch (err) {
      console.error(err);
      throw HttpStatus.BAD_REQUEST;
    }
  }

  findByTime(time: string) {
    return this.telegramUsersRepository.find({ where: { time } });
  }

  deleteUser(userId: number) {
    return this.telegramUsersRepository.delete({ userId });
  }
}
