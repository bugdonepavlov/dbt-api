import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TelegramBotService } from './telegram.service';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';

@ApiTags('Telegram')
@Controller({
  path: 'telegram',
  version: '1',
})
@UseInterceptors(TransformationInterceptor)
export class TelegramController {
  constructor(private service: TelegramBotService) {}

  @Get('users/:time')
  @HttpCode(HttpStatus.OK)
  findAllByTime(@Param('time') time: string) {
    return this.service.findByTime(time);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') userId: string) {
    return this.service.findOneByUserId(+userId);
  }

  @Post(':id')
  @HttpCode(HttpStatus.OK)
  create(@Param('id') userId: string) {
    return this.service.saveTelegramUser(+userId);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  removeUser(@Param('id') userId: string) {
    return this.service.deleteUser(+userId);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  updateUser(
    @Param('id') userId: string,
    @Body() params: { time: string; timezone?: string },
  ) {
    return this.service.updateTelegramUser(+userId, params);
  }
}
