import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { EntityHelper } from 'src/utils/entity-helper';

@Entity()
export class TelegramUser extends EntityHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  timezone: string;

  @Column()
  time: string;

  @Column()
  userId: number;
}
