import { Module } from '@nestjs/common';
import { TelegramBotService } from './telegram.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TelegramUser } from './telegram-user.entity';
import { TelegramController } from './telegram.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TelegramUser])],
  providers: [TelegramBotService],
  controllers: [TelegramController],
  exports: [TelegramBotService],
})
export class TelegramModule {}
