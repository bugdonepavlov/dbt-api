import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { DataSource, DataSourceOptions } from 'typeorm';

import databaseConfig from './config/database.config';
import authConfig from './config/auth.config';
import appConfig from './config/app.config';
import mailConfig from './config/mail.config';
import fileConfig from './config/file.config';

import { UsersModule } from './users/users.module';
import { AnswersModule } from './answers/answers.module';
import { FilesModule } from './files/files.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmConfigService } from './database/typeorm-config.service';
import { ForgotModule } from './forgot/forgot.module';
import { MailModule } from './mail/mail.module';
import { SessionModule } from './session/session.module';
import { MailerModule } from './mailer/mailer.module';
import { PollsModule } from './polls/polls.module';
import { PatientsModule } from './patients/patients.module';
import { DoctorsModule } from './doctors/doctors.module';
import { DoctorsNotesModule } from './doctor_notes/doctor_notes.module';
import { TrainersModule } from './trainers/trainers.module';
import { TelegramModule } from './telegram/telegram.module';

import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import dayjs from 'dayjs';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault('Europe/Moscow');

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [databaseConfig, authConfig, appConfig, mailConfig, fileConfig],
      envFilePath: ['.env'],
    }),
    TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
      dataSourceFactory: async (options: DataSourceOptions) => {
        return new DataSource(options).initialize();
      },
    }),
    DoctorsModule,
    PatientsModule,
    DoctorsNotesModule,
    PollsModule,
    AnswersModule,
    UsersModule,
    FilesModule,
    AuthModule,
    ForgotModule,
    SessionModule,
    MailModule,
    MailerModule,
    TrainersModule,
    TelegramModule,
  ],
})
export class AppModule {}
