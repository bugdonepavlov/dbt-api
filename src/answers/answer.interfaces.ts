export interface IAnswerPayload {
  id: number;
  start: string;
  end: string;
}
