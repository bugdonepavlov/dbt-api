import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';

import { Answer } from './entities/answer.entity';
import { SaveAnswerDto } from './dto/save-answer';
import { IAnswerPayload } from './answer.interfaces';
import { PatientsService } from 'src/patients/patients.service';
import { isIsoDate, parseDate } from './utils';

dayjs.extend(isBetween);
dayjs.extend(utc);
dayjs.extend(timezone);

// dayjs.tz.setDefault('Europe/Moscow');

@Injectable()
export class AnswersService {
  constructor(
    @InjectRepository(Answer)
    private answersRepository: Repository<Answer>,
    private patientService: PatientsService,
  ) {}

  async updateAnswer(id: number, fields: Record<string, string>) {
    const answer = await this.answersRepository.findOne({
      relations: {
        patient: true,
      },
      where: {
        id,
      },
    });

    if (!answer) {
      throw new Error('Ответ не найден');
    }

    return this.answersRepository.save({
      id: id,
      fields,
    });
  }

  async saveAnswers(
    { fields, day }: SaveAnswerDto,
    userId: number,
  ): Promise<Answer> {
    const patient = await this.patientService.findOne({
      user: {
        id: userId,
      },
    });

    if (!patient) {
      throw HttpStatus.BAD_REQUEST;
    }

    const today = dayjs().tz('Europe/Moscow');
    const currentDay = dayjs(day).tz('Europe/Moscow');

    const answer = this.answersRepository.create({
      day,
      fields,
      patient,
      isToday: today.isSame(currentDay, 'date'),
    });

    return this.answersRepository.save(answer);
  }

  async findByUser(userId: number, timezone: string) {
    const answers = await this.answersRepository.find({
      relations: {
        patient: true,
      },
      where: {
        patient: {
          user: {
            id: userId,
          },
        },
      },
    });

    const getDate = (date: string) => {
      if (isIsoDate(date)) {
        const originalTimeZone = date.slice(-6);

        return dayjs(date)
          .utcOffset(originalTimeZone)
          .tz(timezone)
          .toISOString();
      }

      return dayjs(date).tz(timezone).toISOString();
    };

    return answers.map((item) => ({
      ...item,
      day: getDate(item.day),
    }));
  }

  async findById({ id, timezone }) {
    const answer = await this.answersRepository.findOne({
      relations: {
        patient: true,
      },
      where: {
        id,
      },
    });

    if (!answer) {
      throw HttpStatus.BAD_REQUEST;
    }

    const getDate = (date: string) => {
      if (isIsoDate(date)) {
        const originalTimeZone = date.slice(-6);

        return dayjs(date)
          .utcOffset(originalTimeZone)
          .tz(timezone)
          .toISOString();
      }

      return dayjs(date).tz(timezone).toISOString();
    };

    return {
      ...answer,
      day: getDate(answer.day),
    };
  }

  async findAllAnswersByDate({
    start,
    end,
    id,
  }: IAnswerPayload): Promise<Answer[]> {
    const dateStart = dayjs(start)
      .set('minute', 0)
      .set('second', 0)
      .set('hour', 0)
      .toISOString();
    const dateEnd = dayjs(end)
      .set('minute', 59)
      .set('second', 59)
      .set('hour', 23)
      .toISOString();
    // конечную дату на конец дня

    const answers = await this.answersRepository
      .createQueryBuilder('answers')
      .where(`answers.day BETWEEN '${dateStart}' AND '${dateEnd}'`)
      .andWhere(`answers.patient = :id`, { id })
      .getMany();
    const map = new Map<string, Answer>();
    const items = answers.sort(
      (a, b) => new Date(a.day).getTime() - new Date(b.day).getTime(),
    );

    items.forEach((item) => {
      const createdAt = parseDate(item.createdAt);
      const parsedDay = parseDate(item.day);
      const current = Object.assign(item, {
        createdAt,
        day: parseDate(item.day),
        isToday: dayjs(createdAt).isSame(parsedDay, 'date'),
      });

      map.set(parsedDay, current);
    });

    return [...map.values()];
  }
}
