import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EntityHelper } from 'src/utils/entity-helper';
import { Patient } from 'src/patients/entities/patient.entity';

@Entity()
export class Answer extends EntityHelper {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false })
  isToday: boolean = false;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    transformer: {
      from: (value: Date) => value,
      to: (value: string) => new Date(value),
    },
  })
  day: string;

  @Column({
    type: 'json',
    transformer: {
      to(value: Record<string, string>): string {
        return JSON.stringify(value);
      },
      from(value: string): Record<string, string> {
        return JSON.parse(value);
      },
    },
  })
  fields: Record<string, string>;

  @ManyToOne(() => Patient, {
    eager: true,
  })
  patient: Patient;

  @CreateDateColumn()
  createdAt: Date | string;
}
