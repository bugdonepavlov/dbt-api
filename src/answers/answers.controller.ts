import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  SerializeOptions,
  UseGuards,
  Request,
  UseInterceptors,
  Patch,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { AnswersService } from './answers.service';
import { SaveAnswerDto } from './dto/save-answer';
import { QueryAnswerDto } from './dto/query-answer';
import { AuthGuard } from '@nestjs/passport';
import { Answer } from './entities/answer.entity';
import { TransformationInterceptor } from 'src/utils/dataInterceptors';
import { IDayAnswer } from './interfaces';

@ApiTags('Answers')
@Controller({
  path: 'answers',
  version: '1',
})
@UseInterceptors(TransformationInterceptor)
export class AnswersController {
  constructor(private service: AnswersService) {}

  @ApiBearerAuth()
  @Get('patient')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @SerializeOptions({
    groups: ['user'],
  })
  async findAllByUser(
    @Request() request,
    @Query() query: { timezone: string },
  ): Promise<IDayAnswer[]> {
    const timezone = query?.timezone || 'Europe/Moscow';

    const answers = await this.service.findByUser(+request.user.id, timezone);

    return answers.map(({ day, id }: Answer) => ({ day, id }));
  }

  @ApiBearerAuth()
  @Get('patient/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  @SerializeOptions({
    groups: ['user'],
  })
  findById(@Param('id') id: string, @Query() query: { timezone: string }) {
    const timezone = query?.timezone || 'Europe/Moscow';
    return this.service.findById({ id: +id, timezone });
  }

  @ApiBearerAuth()
  @SerializeOptions({
    groups: ['user'],
  })
  @Post('patient')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  saveAnswer(
    @Body() answersDto: SaveAnswerDto,
    @Request() request,
  ): Promise<Answer> {
    return this.service.saveAnswers(answersDto, request.user.id);
  }

  @ApiBearerAuth()
  @SerializeOptions({
    groups: ['user'],
  })
  @Patch('patient/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  updateAnswer(@Param('id') id: string, @Body() body: Record<string, string>) {
    return this.service.updateAnswer(+id, body);
  }

  @ApiBearerAuth()
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findAll(
    @Query() { start, end }: QueryAnswerDto,
    @Param('id') id: string,
  ): Promise<Answer[]> {
    return this.service.findAllAnswersByDate({ id: +id, start, end });
  }
}
