import dayjs from 'dayjs';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);
dayjs.extend(timezone);

export const parseDate = (date: string | Date): string => {
  let val = date;

  if (typeof val === 'object') {
    val = date.toString();
  }

  const originalTimeZone = val.slice(-6);

  return dayjs(val)
    .utcOffset(originalTimeZone)
    .tz('Europe/Moscow')
    .format('DD.MM.YYYY');
};

export const isIsoDate = (str: string) => {
  if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(str)) return false;
  const d = new Date(str);
  return !isNaN(d.getTime()) && d.toISOString() === str; // valid date
};
