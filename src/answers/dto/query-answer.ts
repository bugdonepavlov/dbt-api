import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class QueryAnswerDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  start: string;

  @IsOptional()
  end: string;
}
