import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SaveAnswerDto {
  @ApiProperty()
  @IsNotEmpty()
  fields: Record<string, string>;

  @ApiProperty()
  @IsNotEmpty()
  day: string;
}
